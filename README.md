#### Code for Semantic Web Technology project 2018/2019 ####

__QA_system using SpaCy, Lango, SPARQL and Wikidata__


__Team members:__

* Roy David
* Leon Graumans
* Wessel Reijngoud
* Mike Zhang

__Instructions:__

Commands to run system:

* python3 qasystem.py
* cat [question-file] | python3 qasystem.py


__StanfordServerparser__

In order to run the StanfordServerParser:

* Download StanfordCore NLP at https://stanfordnlp.github.io/CoreNLP/#download
* Run the following command in the folder where you extracted Stanford CoreNLP:
* java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer




