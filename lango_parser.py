#!/usr/bin/python3

# runquery.py
# file for parsing using Lango
# 10-11-2018
#
# Roy David, Leon Graumans, Wessel Reijngoud, Mike Zhang
# Revisions: 1
#
# To run the StanfordServerParser
# open the stanforddir in your terminal and paste the following line:
# java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer
#
# In this file, the Lango matcher is used to match parsed trees with rules


from lango.parser import StanfordServerParser
from lango.matcher import match_rules
from collections import OrderedDict
from nltk.stem import WordNetLemmatizer

parser = StanfordServerParser('localhost', 9000, {})

"""
Rule for matching a subject and/or property of NP
Matches:
- subject            : Subject to get property of
- prop     (optional): Propert to get of subject
Examples:
- Obama born
- Obama
- Obama's birthday
- Barack Obama's wife
"""
subj_rules = OrderedDict([
    # When was (Obama born)
    ('( NP ( NP:subject-o ) ( VP:prop-o ) )', {}),
    # What is (the birth day of Obama)
    ('( NP ( NP:prop-o ) ( PP ( IN ) ( NP:subject-o ) ) )', {}),
    # What is (Obama's birthday)
    ('( NP ( NP:subject-o ( NNP ) ( POS ) ) ( NN/NNS:prop-o ) $ )', {}),
    # What is (Obama's birth day)
    ('( NP ( NP:subject-o ( NNP ) ( POS ) ) ( NN/JJ:prop-o ) ( NN/NNS:prop2-o ) )', {}),
    # What is (Barrack Obama's birthday)
    ('( NP ( NP:subject-o ( NNP ) ( NNP ) ( POS ) ) ( NN/NNS:prop-o ) $ )', {}),
    # What is (Barack Obama's birth day)
    ('( NP ( NP:subject-o ( NNP ) ( NNP ) ( POS ) ) ( NN/JJ:prop-o ) ( NN/NNS:prop2-o ) )', {}),
    ('( NP:subject-o )', {}),
])

"""
Rule for matching subject property query
Matches:
- qtype               : Question type (who, where, what, when)
- subject             :  Subject to get property of
- prop      (optional): Property to get of subject
- prop2     (optional): Second part of property
- prop3     (optional): Overwrite property
- jj        (optional): Adjective that will be property (e.g. many/tall/high)
Examples:
- What religion is Obama?
- Who did Obama marry?
- Who is Obama?
- Who is Barack Obama's wife?
- How tall is Mt. Everest?
"""
subject_prop_rules = {
    '( SBARQ ( WHNP/WHADVP/WHADJP/WHPP:qtype_t ) ( SQ:sq_t ) )': {
        'qtype_t': OrderedDict([
            # What religion
            ('( WHNP ( WDT:qtype-o=what ) ( NN:prop3-o ) )', {}),
            # Which religion
            ('( WHNP ( WDT:qtype-o=which ) ( NN:prop3-o ) )', {}),
            # How many/tall
            ('( WHADJP ( WRB:qtype-o ) ( JJ:jj-o ) )', {}),
            # What/where/who
            ('( WHNP/WHADVP:qtype-o )', {}),
            # After what
            ('( WHPP ( IN:prop2-o ) ( WHNP/WHADVP:qtype-o=what ) )', {}),
            # In which year
            ('( WHPP ( IN ) ( WHNP ( WDT:qtype-o=which ) ( NN:prop2-o ) ) )', {}),
        ]),
        'sq_t': {
            # What ethnicity is Obama
            '( SQ ( VP ( ADVP:prop-o ) ) ( VBZ ) ( VP:suject-o ) )': {},
            # Who did Obama marry
            '( SQ ( VBZ/VBD/VBP:action-o ) ( NP:subj_t ) ( VP:prop-o ) )': {
                'subj_t': subj_rules
            },
            # Who did 
            '( SQ ( VP ( VBZ/VBD/VBP:action-o ) ( NP:subj_t ) ) )': {
                'subj_t': subj_rules
            },
            # Who is Edward Thatch known as
            '( SQ ( VBZ:action-o ) ( NP:subj_t ) ( VP:prop-o ) )': {
                'subj_t': subj_rules,
            },
            # What is Obama
            '( SQ ( VBZ/VBD/VBP:action-o ) ( NP:subj_t ) )': {
                'subj_t': subj_rules
            },
            # What fish is used for gravlax
            '( SQ ( VBZ:action-o ) ( VP ( VBN:prop-o=used ) ( PP ( IN ) ( NP:subj_t ) ) ) )': {
                'subj_t': subj_rules
            },
            # What is used to make tea?
            '( SQ ( VBZ:action-o ) ( VP ( VBN:prop-o=used ) ( S ( VP ( TO ) ( VP ( VB ) ( NP:subj_t ) ) ) ) ) )': {
                'subj_t': subj_rules,
            },
            # What is in a cookie?
            '( SQ ( VBZ:action-o ) ( PP ( IN:prop-o=in ) ( NP:subj_t ) ) )': {
                'subj_t': subj_rules
            },
        }
    },
    # Is Obama female?
    '( SQ ( VBZ:qtype-o=is ) ( NP:subj_t ) ( NP/ADJP:action-o ) )': {
                'subj_t': subj_rules
            },
    # List the ingredients of apple pie
    '( S ( VP ( VB:qtype-o=list ) ( NP ( NP:action-o ) ( PP ( IN ) ( NP:subj_t ) ) ) ) )': {
                'subj_t': subj_rules,
            },
    # Give the ingredients of apple pie
    '( S ( VP ( VB:qtype-o=give ) ( NP ( NP:action-o ) ( PP ( IN ) ( NP:subj_t ) ) ) ) )': {
                'subj_t': subj_rules,
            },
    # Do hamburgers contain meat?
    '( SQ ( VP ( VB:qtype-o=do ) ( SBAR ( S ( NP:subj_t ) ( VP ( VBP:action-o ) ( NP:prop-o ) ) ) ) ) )': {
                'subj_t': subj_rules,
            },
    # Does teriyaki originate from China?
    # Is goat milk produced by male goats?
    '( SQ ( VBZ:qtype-o ) ( NP:subj_t ) ( VP ( VB/VBN:action-o ) ( PP ( IN ) ( NP:prop-o ) ) ) )': {
                'subj_t': subj_rules,
            },
    # Does meat contain muscle?
    '( S ( S ( VP ( VBZ/VB:qtype-o ) ( NP:subj_t ) ) ) ( VP ( VBP:action-o ) ( NP:prop-o ) ) )': {
                'subj_t': subj_rules,
            },
}


def subject_fun(qtype, subject, action, jj=None, prop=None, prop2=None, prop3=None):
    '''Function to replace variables'''

    # print('%s, %s, %s, %s, %s, %s, %s' % (qtype, subject, action, prop, prop2, prop3, jj))

    if jj == 'old':
        prop = 'age'
    elif jj in ['tall', 'high']:
        prop = 'height'

    if prop and prop2:
        prop = prop + ' ' + prop2
    if prop2 and not prop:
        prop = prop2
    if prop3 and not prop:
        prop = prop3

    if prop == 'born':
        prop = 'birthday' if qtype == 'when' else 'birthplace'
    elif prop == 'location' and 'headquarters of ' in subject:
        prop = subject[:12]
        subject = subject[16:]
    elif prop in ['founded', 'invented', 'come from']:
        prop = 'inception' if qtype == 'when' else 'originates'
    elif prop in ['company', 'producer', 'produces']:
        prop = 'manufacturer'
    elif prop in ['used', 'in', 'contain', 'contains']:
        prop = 'has part'

    if qtype.startswith('how m'):
        prop = qtype[9:]
        qtype = 'count'
    elif prop and qtype in ['is', 'does', 'do', 'has']:
        qtype = prop
        prop = action
    
    if not prop and action in ['is', 'was']:
        prop = action
    if not prop and qtype in ['is', 'was', 'does', 'do', 'has']:    # is Obama female?
        prop = action
    if not prop and qtype in ['list', 'give']:                      # list/give the ingredients of apple pie
        prop = action
        qtype = 'list'
    whitelist = ['causes']
    if not prop and action in whitelist:
        prop = WordNetLemmatizer().lemmatize(action)

    elif prop in ['used', 'in', 'contain']:
        prop = 'has part'

    return (qtype, subject, prop)


def langoparser(line):
    low = lambda s: s[:1].lower() + s[1:] if s else ''              # lowercase first letter of string
    line = low(line)

    tree = parser.parse(line)
    # print(tree)
    lango_output = match_rules(tree, subject_prop_rules, subject_fun)

    return lango_output




