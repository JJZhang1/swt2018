#!/usr/bin/python3

# qasystem.py
# main file for question answering
# 10-11-2018
#
# Roy David, Leon Graumans, Wessel Reijngoud, Mike Zhang
# Revisions: 1
#
# command:
#   1. [interactive] >> python3 qasystem.py
#   2. cat [question-file] | python3 qasystem.py

import sys
import string
from nltk.stem import SnowballStemmer
from nltk.stem import WordNetLemmatizer

# local files
from spacy_parser import spacyparser
from lango_parser import langoparser
from runquery import find_IDs, find_alt_ID, find_binary_IDs

stemmer = SnowballStemmer('english')
lemmatizer = WordNetLemmatizer()

def return_error():
    '''Function if it is not possible to find an answer'''

    print('Couldn\'t find an answer, please try another question.')


def send_query(tup, line, state=True):
    '''Function takes tuple returned from parsers and fires a SPARQL query'''

    first_word = line.split(' ', 1)[0].lower()
    qtype, subject, prop = tup
    ans = None

    # print('\n-- question: ', line)

    subject = lemmatizer.lemmatize(subject)
    subject = subject.strip(string.punctuation)
    if not state and len(subject) - len(stemmer.stem(subject)) < 2:
        subject = stemmer.stem(subject)

    what_list = ['what', 'give', 'list', 'where', 'which', 'who', 'whom', 'when', 'how', 'name']
    count_list = ['count', 'how many']
    yesno_list = ['is', 'was', 'are', 'do', 'does', 'could', 'should']


    if qtype in what_list:
        if prop not in yesno_list:
            # print('Running normal query...')
            ans = find_IDs(subject, prop)                                           # What are the ingredients of...
            if ans:
                for item in ans: print(item)
        else:
            # print('Running descriptive query...')
            ans = find_alt_ID(subject)                                              # Who is...
            print(ans) if ans else return_error()


    elif qtype in count_list:
        # print('Running count query...')
        ans = find_IDs(subject, prop)
        if ans:
            if ans[0].isdigit():                                                    # How many employees...
                print(ans[-1])
            else:                                                                   # How many ingredients...
                print(len(ans))
                                                      

    elif first_word in yesno_list:
        # print('Running binary query...')
        if qtype in yesno_list:
            ans = find_binary_IDs(subject, prop)                                    # Is Jamie Oliver female/male?
            print('Yes') if ans else print ('No')
        else:
            ans = find_IDs(subject, prop)
            if ans:
                ans = [token.lower() for token in ans]
                print('Yes') if qtype in ans else print('No')                       # Does meat contain muscle?
            else:
                print('No')
        return



    if state and not ans:                                                           # If no ans, try inverse tuple...
        tup = (qtype, prop, subject)
        send_query(tup, line, state=False)
    elif not state and not ans:
        return_error()


def main(argv):

    print('Please, enter your query below. If no query is given, the system will exit.\n')

    for line in sys.stdin:
        line = line.rstrip()

        if not line:
            print('No input...')
            raise SystemExit
        else:
            print('\n', line)
            lango_output = langoparser(line)
            spacy_output = spacyparser(line)
            if len(spacy_output) > 3:                                   # if spacytuple > 3, concatenate last elements
                spacy_output = (spacy_output[0], spacy_output[1], ' '.join(spacy_output[2:]))

            if not lango_output or None in lango_output:
                try:
                    # print('Using SpaCy query...')
                    send_query(spacy_output, line)
                except Exception:
                    try:
                        # print('Incomplete Lango output, try inverse SpaCy query...')
                        spacy_output_inverse = (spacy_output[0], spacy_output[2], spacy_output[1])
                        send_query(spacy_output_inverse, line)
                    except Exception:
                        return_error()

            else:
                # print('Using Lango query...')
                send_query(lango_output, line)



if __name__ == '__main__':
    main(sys.argv)
