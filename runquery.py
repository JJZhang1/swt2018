#!/usr/bin/python3

# runquery.py
# file for firing SPARQL queries
# 10-11-2018
#
# Roy David, Leon Graumans, Wessel Reijngoud, Mike Zhang
# Revisions: 1
#

import requests
from operator import itemgetter
from utils import compute_LD

sparqlurl = 'http://query.wikidata.org/sparql'

def run_query(entity_id, relation_id):
    '''When the entity ID and the relation ID are found we can run the query on http://query.wikidata.org/sparql'''

    query = 'SELECT ?itemLabel WHERE { wd:'+entity_id+' wdt:'+relation_id+' ?item. SERVICE wikibase:label {bd:serviceParam wikibase:language "en" .} }'
    results = requests.get(sparqlurl, params={'query': query, 'format': 'json'}).json()

    answer = []
    for result in results["results"]["bindings"]:
        for arg in result:
            answer.append(result[arg]["value"])
    return answer


def run_alt_query(entity_id):
    '''Get description of entity'''

    query = 'SELECT ?item WHERE { wd:' + entity_id + '  schema:description   ?item. FILTER (LANG(?item)="en")}'
    results = requests.get(sparqlurl, params={'query': query, 'format': 'json'}).json()

    for item in results['results']['bindings']:
        for var in item:
            return (item[var]['value']) if item[var]['type'] == 'literal' else (var, item[var])

def run_binary_query(id1,  id2):
    '''When the entity ID's and the relation ID are found we can the yes-or-no query'''

    query = 'ASK WHERE { { SELECT DISTINCT ?item ?itemLabel WHERE { wd:'+id2+' wdt:P31 ?class. ?class wdt:P1687 ?prop. BIND (IRI(Replace(str(?prop), str(wd:), str(wdt:))) as ?propAsT). wd:'+id1+' ?propAsT ?gender. SERVICE wikibase:label {bd:serviceParam wikibase:language "en" .} } } }'
    results = requests.get(sparqlurl, params={'query': query, 'format' : 'json'}).json()

    return results['boolean']

def count_languages(entity_id):
    '''Function that calculates the amount of translations of an entity page'''

    query = "SELECT (count(?lang) as ?numLang) WHERE { wd:"+entity_id+" rdfs:label ?label . filter(!langmatches(lang(?label), '')) bind(lang(?label) as ?lang) }"
    data = requests.get(sparqlurl, params={'query': query, 'format': 'json'}).json()
    count = int(data['results']['bindings'][0]['numLang']['value'])

    return count


def return_ID(json):
    '''Function that returns list of entities based on json output'''

    try:
        entitylist = []
        for result in json['search']:
            entity_id = result['id']
            count = count_languages(entity_id)
            entitylist.append((entity_id, count))
        return entitylist
    except Exception:
        return None

def find_IDs(entity, relation):
    '''Function to find relation and entity id's'''

    wdapi = 'https://www.wikidata.org/w/api.php'
    wdparams = {'action':'wbsearchentities', 'language':'en', 'format':'json'}

    wdparams['search'] = entity
    json = requests.get(wdapi, wdparams).json()
    entitylist = return_ID(json)

    if not entitylist:
        return None

    answerlist = []
    for ID in entitylist:

        try:
            # Find the relation ID at wikiData
            wdparams['search'] = relation
            wdparams['type'] = 'property'
            json = requests.get(wdapi, wdparams).json()
            if not json['search']:
                new_id = compute_LD(relation)
                alt_ans = run_query(ID[0], new_id)
                answerlist.append(alt_ans)
            else:
                for result in json['search']:
                    relation_id = result['id']
                    ans = run_query(ID[0], relation_id)
                    if ans:
                        answerlist.append(ans)
        except Exception:
            pass

    # Return answer with most items
    return None if not answerlist else max(answerlist, key=len)


def find_alt_ID(entity, ignore=None):
    '''Find id for entity, ignore disambiguation pages'''

    wdapi = 'https://www.wikidata.org/w/api.php'
    wdparams = {'action':'wbsearchentities', 'language':'en', 'format':'json'}

    wdparams['search'] = entity
    json = requests.get(wdapi,wdparams).json()
    entitylist = []
    for result in json['search']:
        ID = result['id']
        if ignore and ignore == ID:
            continue
        count = count_languages(ID)
        entitylist.append((ID, count))
    entity_id = max(entitylist,key=itemgetter(1))[0] if entitylist else None  

    if not entitylist:
        return None

    ans = run_alt_query(entity_id)

    # Recursive call if Wikimedia disambiguation page
    return find_alt_ID(entity, entity_id) if 'disambiguation' in ans else ans


def find_binary_IDs(entity1, entity2):
    '''Function for yes and no queries with two entities'''

    wdapi = 'https://www.wikidata.org/w/api.php'
    wdparams = {'action':'wbsearchentities', 'language':'en', 'format':'json'}

    # Find the entity1 ID at wikiData
    wdparams['search'] = entity1
    json = requests.get(wdapi,wdparams).json()
    entitylist = return_ID(json)
    entity1_id = max(entitylist,key=itemgetter(1))[0] if entitylist else None

    # Find the entity2 ID at wikiData
    wdparams['search'] = entity2
    json = requests.get(wdapi,wdparams).json()
    entitylist = return_ID(json)
    entity2_id = max(entitylist, key=itemgetter(1))[0] if entitylist else None

    if not entity1_id or not entity2_id:
        return None

    return True if run_binary_query(entity1_id, entity2_id) else False
