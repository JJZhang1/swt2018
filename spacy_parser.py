#!/usr/bin/python3

# runquery.py
# file for parsing using SpaCy
# 10-11-2018
#
# Roy David, Leon Graumans, Wessel Reijngoud, Mike Zhang
# Revisions: 1
#

import en_core_web_sm

nlp = en_core_web_sm.load()

def find_qtype(w,check_line):
    '''Find question type'''

    if w.tag_ == 'WP' or w.tag_ == 'WRB' or w.tag_ == 'WDT':
        return w.text
    elif w.tag_.startswith('VB') and w.dep_ == 'ROOT' and w.text == check_line[0]:
        return w.text
    elif w.dep_ == 'amod':
        return w.text
    else:
        return

def find_subj(w):
    '''Find main entity of the sentence'''

    if w.dep_ == 'nsubj' and w.tag_.startswith('NN'):
        return w.text
    elif w.dep_ == 'compound' and w.tag_.startswith('NN'):
        return w.text
    elif w.dep_ == 'pobj' and w.tag_.startswith('NN'):
        return w.text
    elif w.dep_ == 'pcomp' and w.tag_.startswith('NN'):
        return w.text
    else:
        return

def find_prop(w,check_line):
    '''Find property/relation of the sentence'''

    if w.dep_ == 'pobj' or w.dep_ == 'dobj':
        return w.text
    elif w.dep_ == 'attr' and w.tag_.startswith('NN'):
        return w.text
    elif w.dep_ == 'acomp' and w.tag_.startswith('NN'):
        return w.text
    elif w.tag_.startswith('VB') and w.dep_ == 'ROOT' and w.text == check_line[-1]:
        return w.text
    else:
        return

def concatenator(spql):
    '''Concatenates names/entities if list is longer than three'''

    sen = ' '.join(spql)
    res = nlp(sen)
    com = []
    total = []
    for index, w in enumerate(res):
        # print(w, w.tag_, w.dep_, w.ent_type_)
        if w.ent_type_ == 'ORG' and w.text != res[-1].text and res[index + 1].ent_type_ == 'ORG':
            com.append(w.text)
        elif w.ent_type_ == 'ORG' and res[index - 1].ent_type_ == 'ORG':
            com.append(w.text)
        elif w.ent_type_ == 'PERSON' and w.text != res[-1].text and res[index + 1].ent_type_ == 'PERSON':
            com.append(w.text)
        elif w.ent_type_ == 'PERSON' and res[index - 1].ent_type_ == 'PERSON':
            com.append(w.text)
        elif w.ent_type_ == 'NORP' and w.text != res[-1].text and res[index + 1].ent_type_ == 'NORP':
            com.append(w.text)
        elif w.ent_type_ == 'NORP' and res[index - 1].ent_type_ == 'NORP':
            com.append(w.text)

        elif w.dep_ == 'advmod' and w.text != res[-1].text and res[index + 1].dep_ == 'amod':
            com.append(w.text)
        elif w.dep_ == 'amod' and res[index - 1].dep_ == 'advmod':
            com.append(w.text)

        elif w.tag_ == 'JJ' and w.text != res[-1].text and res[index + 1].tag_ == 'NN':
            com.append(w.text)
        elif w.tag_ == 'NN' and res[index - 1].tag_ == 'JJ':
            com.append(w.text)

        elif w.tag_ == 'VBP':
            continue

        else:
            total.append(w.text)

    total.append(' '.join(com))

    return total

def spacyparser(line):
    '''this function makes use of the spacy package, specifically made use of the dependency (.dep_)
    and (.tag_) attribute of this package to find dependencies between words.'''

    spql = [] # qtype - subj - prop
    check_line = line[:-1].split()  # get rid of question mark
    result = nlp(line)
    for w in result:
        qtype = find_qtype(w, check_line)
        spql.append(qtype)

        subj = find_subj(w)
        spql.append(subj) if subj not in spql else None

        prop = find_prop(w, check_line)
        spql.append(prop) if prop not in spql else None

    spql = [x for x in spql if x is not None]

    if len(spql) > 3:
        total = concatenator(spql)
        return tuple([x.lower() for x in total if x != ''])
    else:
        return tuple([x.lower() for x in spql])

