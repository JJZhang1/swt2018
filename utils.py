import json
import pprint
import operator

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]


def compute_LD(relation):
    relation = 'occupations'
    with open('props.json') as f:
        data = json.load(f)


    low = 10
    var = ''
    for key in data:
        if key[0] != relation[0]:
            continue
        distance = levenshtein(relation, key)
        if distance < low:
            low = distance
            var = (key)

    return max(data[var].items(), key=operator.itemgetter(1))[0]

    
