#!/usr/bin/python3

#https://www.analyticsvidhya.com/blog/2015/10/beginner-guide-web-scraping-beautiful-soup-python/
import requests
from bs4 import BeautifulSoup
import pickle
import sys
import json


sys.setrecursionlimit(40000)

url = 'https://www.wikidata.org/wiki/Wikidata:Database_reports/List_of_properties/all'
response = requests.get(url)
html = response.content
def main():
    soup = BeautifulSoup(html, "html.parser")
    right_table = soup.find('table', { "class" : 'wikitable sortable'})
    #Generate lists
    A=[]
    B=[]
    C=[]
    D=[]
    E=[]
    F=[]
    for row in right_table.findAll("tr"):
        cells = row.findAll('td')
        states=row.findAll('th') #To store second column data
        if len(cells)==6: #Only extract table body not heading
            A.append(cells[0].find(text=True))
            B.append(cells[1].find(text=True))
            C.append(cells[2].find(text=True))
            D.append(cells[3].find(text=True))
            E.append(cells[4].find(text=True))
            F.append(cells[5].find(text=True))
    #import pandas to convert list to data frame
    import pandas as pd
    df=pd.DataFrame(A,columns=['ID'])
    df['label']=B
    # df['description']=C
    df['aliases']=D
    # df['Datatype']=E
    df['Count']=F

    myd = {}
    for a,b,c,d in zip(df.ID, df.label, df.aliases, df.Count):
        d = d.strip("\n")
        # d = d.strip(",")
        d = d.replace(",", "")
        d = int(d)
        mydtemp = {}
        mydtemp[a] = d
        if b in myd:
            myd[b].update(mydtemp)
        else:
            myd[b] = mydtemp
        for word in str(c).split(', '):
            if word in myd:
                myd[word].update(mydtemp)
            else:
                myd[word] = mydtemp

    print(myd['media'])



    """Dumps a pickle in a jar"""
    # pickle.dump(myd, open("rick.pickle", "wb"))
    # with open('rick.pickle', 'wb') as fp:
    #     pickle.dump(myd, fp, protocol=pickle.HIGHEST_PROTOCOL)

    with open('props.json', 'w') as fp:
        json.dump(myd, fp)
    """Opens a pickle and prints it"""
    # file = open("dict.p",'rb')
    # object_file = pickle.load(file)
    # print(object_file)
main()
